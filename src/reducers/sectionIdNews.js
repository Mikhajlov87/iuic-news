import {
  GET_SECTION_ID_NEWS,
  GET_SECTION_ID_NEWS_PENDING,
  GET_SECTION_ID_NEWS_FULFILLED,
  GET_SECTION_ID_NEWS_REJECTED,
  GET_CITY_NEWS,
  GET_CITY_NEWS_PENDING,
  GET_CITY_NEWS_FULFILLED,
  GET_CITY_NEWS_REJECTED } from '../actions'

const initialState = {
  content: [],
  pending: false,
  error: false
};

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_SECTION_ID_NEWS_FULFILLED:
      return { ...state,
        content: action.payload.data.data,
        pending: false,
        error: false
      }
    case GET_SECTION_ID_NEWS_PENDING:
      return { ...state,
        pending: true,
        error: false
      }
    case GET_SECTION_ID_NEWS_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    case GET_CITY_NEWS_FULFILLED:
      return { ...state,
        content: action.payload.data.data,
        pending: false,
        error: false
      }
    case GET_CITY_NEWS_PENDING:
      return { ...state,
        pending: true,
        error: false
      }
    case GET_CITY_NEWS_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    default:
      return state
  }
}