import {
  GET_USER_ADDRESS_PENDING,
  GET_USER_ADDRESS_FULFILLED,
  GET_USER_ADDRESS_REJECTED
} from '../actions'

const initialState = {
  address: {},
  pending: false,
  error: false
};

export default function newsState(state = initialState, action) {
  switch(action.type) {
    case GET_USER_ADDRESS_PENDING:
      return { ...state,
        pending: true
      }

    case GET_USER_ADDRESS_REJECTED:
      return {...state,
        pending: false,
        error: true
      }

    case GET_USER_ADDRESS_FULFILLED:
      const data = action.payload;
      return { ...state,
        address: {
          countryNameRu: data.attributes.name_ru,
          countryID: data.id,
          cities: data.attributes.cities
        },
        pending: false,
        error: false
      }
    default:
      return state;
  }
}