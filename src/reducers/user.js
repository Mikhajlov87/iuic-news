import {
  GET_USER_INFO_PENDING,
  GET_USER_INFO_FULFILLED,
  GET_USER_INFO_REJECTED } from '../actions'

const initialState = {
  info: {},
  pending: false,
  error: false
};

export default function newsState(state = initialState, action) {
  switch(action.type) {
    case GET_USER_INFO_PENDING:
      return { ...state,
        info: [],
        pending: true
      }

    case GET_USER_INFO_REJECTED:
      return {...state,
        pending: false,
        error: true
      }

    case GET_USER_INFO_FULFILLED:
      let data = action.payload.data.data.attributes;
      return { ...state,
        info: data,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}