import {
  SUBSCRIBE_USER_INFO_PENDING,
  SUBSCRIBE_USER_INFO_REJECTED,
  SUBSCRIBE_USER_INFO_FULFILLED
} from '../actions';

const logged = window.localStorage.getItem("logged"),
  userId = window.localStorage.getItem("userId"),
  userFirstName = window.localStorage.getItem("userFirstName"),
  userLastName = window.localStorage.getItem("userLastName"),
  userAvatar = window.localStorage.getItem("userAvatar");

const initialState = {
  logged: !!logged ? logged : false,
  userID: !!userId ? userId : null,
  userFirstName: !!userFirstName ? userFirstName : "Жарь-Лук",
  userLastName: !!userLastName ? userLastName : "де Блюю",
  userAvatar: !!userAvatar ? userAvatar : "/user-default.jpg"
}

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case SUBSCRIBE_USER_INFO_PENDING:
      return { ...state,
        pending: true,
        error: false
      }

    case SUBSCRIBE_USER_INFO_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    case SUBSCRIBE_USER_INFO_FULFILLED:
      let data = action.payload.data.data,
        localStor = window.localStorage;

      localStor.setItem('logged', true);
      localStor.setItem('userId', data.id);
      localStor.setItem('userFirstName', data.attributes.first_name);
      localStor.setItem('userLastName', data.attributes.last_name);
      localStor.setItem('userAvatar', data.attributes.avatar_link);

      return {...state,
        logged: true,
        userID: data.id,
        userFirstName: data.attributes.first_name,
        userLastName: data.attributes.last_name,
        userAvatar: data.attributes.avatar_link,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}