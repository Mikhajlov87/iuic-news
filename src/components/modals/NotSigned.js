import React, { Component } from 'react';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';

class NotSigned extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return(
      <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>Ошибка!</ModalHeader>
        <ModalBody>
          <p>Создавать Новость могут только зарегистрированные пользователи!</p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default NotSigned;