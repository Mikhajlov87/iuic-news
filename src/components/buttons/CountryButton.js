import React, { Component } from 'react'
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import axios from 'axios'

class CountryButton extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.searchCountry = this.searchCountry.bind(this);
    this.selectCountry = this.selectCountry.bind(this);

    this.state = {
      dropdownOpen: false,
      countries: [],
      searchCountryValue: '',
      searchCountryPreviusValue: 'выбор страны',
      searchButton: 'выбор страны',
      searchButtonDefaultValue: ''
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
    if (typeof this.state.searchButton === 'string') {
      this.setState({
          searchButton:
              <input
                autoFocus
                className="country-button__search-field"
                type="text"
                onChange={this.searchCountry}>
              </input>
        });
    } else {
      this.setState({
        searchButton: this.state.searchCountryPreviusValue,
        searchCountryValue: ''
      })
    }
  }

  selectCountry(event) {
    let url = "http://news.iuic.info/v1/web/countries/" + event.target.value,
      newsUrl = url + "/news/";

    this.props.cities(url);
    this.props.onGoAwayNews();
    this.props.countryNews(1, 16, newsUrl);

    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      searchButton: event.target.textContent,
      searchCountryPreviusValue: event.target.textContent
    });
  }

  searchCountry(event) {
    this.setState({
      searchCountryValue: event.target.value
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      countries: nextProps.countries.countries,
      searchButtonDefaultValue: nextProps.userCountry ? nextProps.userCountry : "выбор страны"
    })
    if (nextProps.userCountry && this.state.searchButton === "выбор страны") {
      this.setState({
        searchButton: nextProps.userCountry,
        searchCountryPreviusValue: nextProps.userCountry
      });
    }
  }

  render() {
    let countries = this.state.countries;
    return(
      <ButtonDropdown
        className="country-button__container"
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}>
        <DropdownToggle className="country-button__btn">
          {this.state.searchButton}
        </DropdownToggle>
        <DropdownMenu onClick={this.selectCountry}>
          {countries.map((item, key) => {
            if ( item.name_ru.toLowerCase().indexOf( (this.state.searchCountryValue).toLowerCase() ) !== -1 ) {
              return <DropdownItem key={key} value={item.id}>{item.name_ru}</DropdownItem>
            }
          })}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

export default CountryButton;