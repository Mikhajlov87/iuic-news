import React, { Component } from 'react';
import InfoPanel from './InfoPanel';
import axios from 'axios';

class CityInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: '',
      city: '',
      cityImg: '',
      currency: '',
      coatImg: '',
      phoneCode: '',
      population: ''
    }
  }
  componentDidMount() {
    if (this.props.city_id) {
      const _this = this;
        let server = "http://news.iuic.info";
        let cityId = this.props.city_id;
        let url = server + "/v1/web/cities/" + cityId;

        axios.get(url)
          .then(function (response) {
            let cityInfo = response.data.data.attributes;
            _this.setState({
              country: cityInfo.country_name_ru,
              city: cityInfo.name_ru,
              currency: cityInfo.currency,
              cityImg: server + cityInfo.city_cover_img.url,
              coatImg: server + cityInfo.coat_img.url,
              phoneCode: cityInfo.phone_code,
              population: cityInfo.population
            });
          })
          .catch(function (error) {
            console.log(error);
          });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cityId) {
      const _this = this;
      let server = "http://news.iuic.info";
      let cityId = nextProps.cityId;
      let url = server + "/v1/web/cities/" + cityId;

      axios.get(url)
        .then(function (response) {
          let cityInfo = response.data.data.attributes;
          _this.setState({
            country: cityInfo.country_name_ru,
            city: cityInfo.name_ru,
            currency: cityInfo.currency,
            cityImg: server + cityInfo.city_cover_img.url,
            coatImg: server + cityInfo.coat_img.url,
            phoneCode: cityInfo.phone_code,
            population: cityInfo.population
          });
        })
        .catch(function (error) {
          console.log(error);
        });
      }
  }

  render() {
    return (
      <div className="city-info">
        <InfoPanel city_info={this.state} />
        <img src={this.state.cityImg} alt="" className="city-info__img" />
      </div>
    );
  }
}

export default CityInfo;