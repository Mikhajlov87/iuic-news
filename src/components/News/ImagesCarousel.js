import React, { Component } from 'react'
import { Carousel } from 'react-responsive-carousel'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../actions'
import TopBar from '../TopBar'
import Footer from '../Footer'

class ImagesCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselItems: []
    }
  }

  componentDidMount() {
    const serverURL = "http://news.iuic.info"
    let sliderItems = [],
      items = this.props.newsItem.screenshots;

    sliderItems.push(
      <img key={this.props.newsItem.title_screenshot} src={serverURL + this.props.newsItem.title_screenshot.url} alt="" />
    )
    items.map((item) => {
      sliderItems.push(
        <img key={item.id} src={serverURL + item.screenshot_img.url} alt="" />
    )
    })
    this.setState({
      carouselItems: sliderItems
    });
  }

  render() {
    return(
      <div>
        <TopBar />
        <Carousel
          className="images-carousel"
          showArrows={true}
          showThumbs={false}
          emulateTouch={true}
          showIndicators={false}
          showStatus={false}
          interval={7000}
          infiniteLoop
          autoPlay>
          {this.state.carouselItems}
        </Carousel>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newsItem: state.newsItem.content
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImagesCarousel)