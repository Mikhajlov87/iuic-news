import React, { Component } from 'react'
import axios from 'axios'
import TimeAgo from 'react-timeago'
import ruStrings from 'react-timeago/lib/language-strings/ru'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../actions'

class TopNews extends Component {
  constructor(props) {
    super(props);
    this.showNews = this.showNews.bind(this);
    this.getNewsItem = this.getNewsItem.bind(this);
    this.state = {
      topNews: []
    }
  }

  getNewsItem(event) {
    const news_id = event.currentTarget.dataset.key;
    this.props.actions.getNewsItem(news_id);
  }

  showNews(state) {
    const formatter = buildFormatter(ruStrings);
    let news = [];
    state.topNews.map( (item, key) => {
      news.push(
        <article key={key} className="news-item__article">
          <Link data-key={item.id} className="news-item__link" to={"/news/"+item.id} onClick={this.getNewsItem}>
            <img className="news-item__img" src={"http://news.iuic.info"+item.img} />
          </Link>
          <Link data-key={item.id} className="news-item__link--description" to={"/news/"+item.id} onClick={this.getNewsItem}>
            <div className="news-item__description">
              {item.title}
            </div>
          </Link>
          <div className="news-item__footer">
            <Link className="news-item__city" to={"/city/"+item.cityId}>
              {item.cityNameRu}
            </Link>
            <TimeAgo className="news-item__date" date={item.date} formatter={formatter} />
          </div>
        </article>
      )
    })
    return news;
  }

  componentDidMount() {
    let url = "http://news.iuic.info/v1/news/top",
      _this = this,
      data = [];
    axios.get(url)
      .then(function(response) {
        response.data.data.map( (item) =>
          data.push({
            id: item.id,
            cityId: item.attributes.city_id,
            cityNameRu: item.attributes.city_name_ru,
            title: item.attributes.title_ru.length > 50 ? item.attributes.title_ru.slice(0, 50) + " ..." : item.attributes.title_ru,
            img: item.attributes.title_screenshot.url,
            date: item.attributes.created_at
          })
        );
        _this.setState({
          topNews: data
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    let isEmptyState = !!this.state.topNews.length;
    return(
      <div className="similar-news">
        <h2 className="similar-news__title">Смотреть еще</h2>
        <div className="similar-news__news-items news-item">
          {isEmptyState ? this.showNews(this.state) : <h3>Loading...</h3> }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newsItem: state.newsItem.content
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TopNews)