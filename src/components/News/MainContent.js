import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
  Card, CardHeader, CardBlock, CardFooter, Button, Alert
} from 'reactstrap'
import Icon from 'react-fontawesome'
import TimeAgo from 'react-timeago'
import ruStrings from 'react-timeago/lib/language-strings/ru'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'
import ReactAudioPlayer from 'react-audio-player'
import Comment from './Comment'

const formatter = buildFormatter(ruStrings);
const serverUrl = "http://news.iuic.info";

class MainContent extends Component {
  constructor(props) {
    super(props);
    this.play = this.play.bind(this);
    this.state = {
      play: "play-button.png",
      playState: true
    }
  }

  play() {
    let audio = this.rap.audioEl;
    if (this.state.playState) {
      this.setState({
        playState: !this.state.playState,
        play: "stop-button.png"
      });
      audio.play();
    } else {
      this.setState({
        playState: !this.state.playState,
        play: "play-button.png"
      });
      audio.pause();
    }
  }

  render() {
    let props = this.props;
    return (
      <Card
        className="main-article main-article__panel"
        tag="article">
        <CardHeader>
          <h2 className="main-article__title">{props.title}</h2>
        </CardHeader>
        <CardBlock>
          <section className="main-article__content clearfix">
            {props.youtube_link ?
              <div className="main-article__video embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" src={props.youtube_link} title="youtube" allowfullscreen></iframe>
              </div>
              : null
            }
            {props.image_count !== 0 ?
              <figure className="main-article__picture picture">
                <Link className="picture__link" to="/screenshots">
                  <img className="picture__img img-thumbnail" src={serverUrl + props.main_image.small.url} alt=""/>
                </Link>
                <figcaption className="picture__actions actions">
                  <div className="actions__show-gallery show-gallery">
                    <span className="show-gallery__count">{props.image_count}&nbsp;</span>
                    <i className="fa fa-camera"></i>
                  </div>
                </figcaption>
              </figure>
              : null
            }
            <div className="main-article__description" dangerouslySetInnerHTML={ {__html:props.description} }></div>
            {props.audio_file ?
              <div className="main-article__audio-player" onClick={this.play}>
                <img src={"/" + this.state.play} alt="audio-player-button" />
                <ReactAudioPlayer
                  onEnded={this.play}
                  src={serverUrl + props.audio_file}
                  ref={(element) => { this.rap = element; }}>
                </ReactAudioPlayer>
              </div>
              : null}
          </section>
        </CardBlock>
        <CardFooter className="main-article__info-panel info-panel">
          <div className="row">
            <div className="col article-card__author author text-left">
              <Link to={props.user_link}>
                <img src={props.user_avatar} className="author__avatar" alt={props.user_name}/>
              </Link>
              <div className="author__info">
                <Link to={props.user_link} className="author__name align-top">{props.user_name}</Link>
                <TimeAgo className="author__date date" date={props.date} formatter={formatter} />
              </div>
            </div>
            <div className="col main-article__info info">
              <Button
                color="link"
                className="info__item info__item--article"
                type="button">
                <Icon name="thumbs-up" />
                <span className="info__count">{props.like}</span>
              </Button>
              <Button
                color="link"
                className="info__item info__item--article"
                type="button">
                <Icon name="thumbs-down" />
                <span className="info__count">{props.dislike}</span>
              </Button>
              <Button
                color="link"
                className="info__item info__item--article"
                type="button">
                <Icon name="star"/>
                <span className="info__count">0</span>
              </Button>
              <Button
                color="link"
                className="info__item info__item--article"
                type="button"
                disabled>
                <Icon name="eye"/>
                <span className="info__count">{props.views}</span>
              </Button>
            </div>
          </div>
        </CardFooter>
        <CardBlock>
          <Alert color="info" className="comments__alert">
            Чтобы оставить комментарий необходимо войти в приложение
          </Alert>
          { props.comments ? <p>Комментарии:</p> : null }
          { props.comments
            ? props.comments.map( (item, index) =>
                <Comment
                  key={index}
                  date={item.created_at}
                  audio={serverUrl + item.audio_file.url}
                  user={item.user_id}>
                </Comment>)
            : null
          }
        </CardBlock>
      </Card>
    );
  }
};

export default MainContent