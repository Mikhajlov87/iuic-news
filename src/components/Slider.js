import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom'
import { map } from 'lodash';

class Slider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const serverUrl = 'http://news.iuic.info';
    const data = this.props.sliderItems.items;
    return(
      <Carousel
        showThumbs={false}
        emulateTouch={true}
        showIndicators={false}
        showStatus={false}
        interval={7000}
        infiniteLoop
        autoPlay
        className="town-carousel">
        { map(data,(item, key)=>(
            <div className="town-carousel__item" style={{backgroundImage: "url("+serverUrl+item.cityImgUrl+")"}} key={key}>
              <div className="town-carousel__city">
                <Link to={'/city/'+item.cityId} className="town-carousel__city-link">{item.cityNameRu}</Link>
              </div>
            </div>
          )) }
      </Carousel>
    );
  }
}

export default Slider;