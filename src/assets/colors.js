//HEADER
export const headerBackground = "#d9edf7";
export const headerMenuButton = "#808080";

//NAVBAR
export const navbarBackground = "#bccecf";
export const navbarLinkColor = "#054d50";