import React from 'react';

const styles = {
  main: {
    alignItems: "center",
    backgroundColor: "#cdcdcd",
    display: "flex",
    height: 300,
    justifyContent: "center"
  }
}

const Main = (props) => (
  <div style={styles.main}>
    {props.children}
  </div>
)

export default Main;