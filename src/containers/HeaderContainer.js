import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MainSlider from '../components/Main/MainSlider';
import MainCountrySlider from '../components/Main/MainCountrySlider';

const HeaderContainer = () => (
  <div className="iuic-news__carousel">
    <Switch>
      <Route exect path="/" component={MainSlider} />
      <Route path="/country" component={MainCountrySlider} />
      <Route path="/city" component={null} />
    </Switch>
  </div>
);

export default HeaderContainer;