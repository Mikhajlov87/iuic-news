import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import TopBar from '../components/TopBar'
import CityInfo from '../components/city-page/info-panel/CityInfo'
import NavBar from '../components/NavBar'
import Rubrificator from '../components/city-page/rubrificator/Rubrificator'
import CityNews from '../components/city-page/CityNews'
import Footer from '../components/Footer'
import * as Actions from '../actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class City extends Component {
  componentDidMount() {
    const city_id = +this.props.location.pathname.match(/\d+/g);
    this.props.actions.getCityNews(city_id)
    console.log(this)
  }
  render() {
    let cityId = +this.props.location.pathname.match(/\d+/g);
    return (
      <div>
        <TopBar />
        <CityInfo city_id={cityId} />
        <NavBar />
        <Container fluid className="custom-offset">
          <Row>
            <Col xl="2" lg="3">
              <Rubrificator
                city_id={cityId}
                match={this.props.match}
                location={this.props.location}>
              </Rubrificator>
            </Col>
            <Col xl="10" lg="9">
              <div className="town-news">
                <CityNews city_id={cityId} />
              </div>
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sectionIdNews: state.sectionIdNews
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(City)